package com.company.paydaytrade.model.dto;

import com.company.paydaytrade.model.entity.Product;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@Component
public class ClientDto {
    List<Product> products;
    Integer total;
    Integer skip;
    Integer limit;
}
