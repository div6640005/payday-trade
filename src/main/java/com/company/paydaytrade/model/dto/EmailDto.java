package com.company.paydaytrade.model.dto;

import com.company.paydaytrade.constants.Messages;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Validated
public class EmailDto {
    @NotNull(message = Messages.EMAIL_IS_URGENT)
    String to;
    @NotNull(message = Messages.SUBJECT_IS_URGENT)
    String subject;
    @NotNull(message = Messages.BODY_IS_URGENT)
    String body;
}
