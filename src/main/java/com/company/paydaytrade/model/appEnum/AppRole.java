package com.company.paydaytrade.model.appEnum;

public enum AppRole {
    APP_USER,
    APP_ADMIN
}
