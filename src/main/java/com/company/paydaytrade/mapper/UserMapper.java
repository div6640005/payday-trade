package com.company.paydaytrade.mapper;

import com.company.paydaytrade.model.dto.AppUserDto;
import com.company.paydaytrade.model.entity.AppUser;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface UserMapper {
    AppUser userDtoToUser(AppUserDto appUserDto);
    AppUserDto userToUserDto(AppUser appUser);

}