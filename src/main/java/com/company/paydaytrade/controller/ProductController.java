package com.company.paydaytrade.controller;

import com.company.paydaytrade.model.dto.PageDto;
import com.company.paydaytrade.model.entity.Product;
import com.company.paydaytrade.service.impl.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/product")
public class ProductController {
    private final ProductServiceImpl productService;

    @GetMapping
    public Page<Product> products(@RequestBody PageDto pageDto) {
        return productService.findAll(pageDto.getSkip(), pageDto.getLimit());
    }

    @GetMapping("/{id}")
    public Optional<Product> findById(@PathVariable Long id) {
        return productService.findById(id);
    }
    @PostMapping
    public ResponseEntity<String> saveProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }
    @PutMapping
    public ResponseEntity<String> updateProduct(@RequestBody Product product){
        return productService.updateProduct(product);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long id){
        return productService.deleteProduct(id);
    }

}
