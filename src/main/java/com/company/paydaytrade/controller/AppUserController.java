package com.company.paydaytrade.controller;

import com.company.paydaytrade.model.dto.AppUserDto;
import com.company.paydaytrade.model.dto.PageDto;
import com.company.paydaytrade.model.entity.AppUser;
import com.company.paydaytrade.service.impl.AppUserServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/appuser")
@Valid
public class AppUserController {
    private final AppUserServiceImpl appUserService;

    @GetMapping
    public Page<AppUser> appUsers(@RequestBody PageDto pageDto) {
        return appUserService.findAll(pageDto.getSkip(), pageDto.getLimit());
    }

    @GetMapping("/{id}")
    public AppUser findById(@PathVariable Long id) {
        return appUserService.findById(id, true);
    }

    @PostMapping
    public ResponseEntity<String> saveAppUser(@RequestBody AppUserDto appUserDto) {
        return appUserService.saveAppUser(appUserDto);
    }

    @PutMapping
    public ResponseEntity<String> updateAppUser(@RequestBody AppUser appUser) {
        return appUserService.updateAppUser(appUser);
    }

    @DeleteMapping("/{email}")
    public ResponseEntity<String> deleteAppUser(@PathVariable String email) {
        return appUserService.deleteAppUser(email);
    }

    @GetMapping("/confirmationEmail/{email}")
    public ResponseEntity<String> confirmationAppUser(@PathVariable String email) {
        return appUserService.emailConfirmation(email);
    }
}
