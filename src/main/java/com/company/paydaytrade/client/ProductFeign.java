package com.company.paydaytrade.client;

import com.company.paydaytrade.model.dto.ClientDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(name = "payday-trade", url = "${client.payday-trade.host}")
public interface ProductFeign {
    @GetMapping("/products")
    ClientDto getAllAvailableCountry();
}