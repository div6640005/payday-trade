package com.company.paydaytrade.repository;

import com.company.paydaytrade.model.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
AppUser findAppUsersByEmail(String email);
AppUser findAppUsersByIdAndIsEnable(Long id, Boolean isEnable);
}
