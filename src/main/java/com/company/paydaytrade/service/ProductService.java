package com.company.paydaytrade.service;

import com.company.paydaytrade.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface ProductService {
    Page<Product> findAll(int skip, int limit);

    Optional<Product> findById(Long id);

    ResponseEntity<String> updateProduct(Product product);

    ResponseEntity<String> deleteProduct(Long id);

    ResponseEntity<String> saveProduct(Product product);
}
