package com.company.paydaytrade.service;

import com.company.paydaytrade.model.dto.AppUserDto;
import com.company.paydaytrade.model.entity.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface AppUserService {
    Page<AppUser> findAll(int skip, int limit);

    AppUser findById(Long id,Boolean isEnable);

    ResponseEntity<String> updateAppUser(AppUser appUser);

    ResponseEntity<String> deleteAppUser(String email);

    ResponseEntity<String> saveAppUser(AppUserDto appUser);
}
