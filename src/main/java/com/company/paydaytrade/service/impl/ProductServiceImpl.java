package com.company.paydaytrade.service.impl;

import com.company.paydaytrade.client.ProductFeign;
import com.company.paydaytrade.model.dto.ClientDto;
import com.company.paydaytrade.model.entity.Product;
import com.company.paydaytrade.repository.ProductRepository;
import com.company.paydaytrade.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ProductFeign productFeign;

    @Override
    public Page<Product> findAll(int skip, int limit) {
        Pageable pageable = PageRequest.of(skip, limit);
        return productRepository.findAll(pageable);
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public ResponseEntity<String> updateProduct(Product product) {
        productRepository.save(product);
        return ResponseEntity.ok("Update successfully !");
    }

    @Override
    public ResponseEntity<String> deleteProduct(Long id) {
        productRepository.deleteById(id);
        return ResponseEntity.ok("Delete successfully !");
    }

    @Override
    public ResponseEntity<String> saveProduct(Product product) {
        productRepository.save(product);
        return ResponseEntity.ok("Save successfully !");
    }

    public void saveProductWithFeign() {
        ClientDto allAvailableCountry = productFeign.getAllAvailableCountry();
        List<Product> products = allAvailableCountry.getProducts();
        for (int i = 0; i < products.size(); i++) {
            saveProduct(products.get(i));
        }
    }
}
