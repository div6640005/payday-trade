package com.company.paydaytrade.service.impl;

import com.company.paydaytrade.constants.Messages;
import com.company.paydaytrade.mapper.UserMapper;
import com.company.paydaytrade.model.dto.AppUserDto;
import com.company.paydaytrade.model.dto.EmailDto;
import com.company.paydaytrade.model.entity.AppUser;
import com.company.paydaytrade.repository.AppUserRepository;
import com.company.paydaytrade.service.AppUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Valid
public class AppUserServiceImpl implements AppUserService {
    private final AppUserRepository appUserRepository;
    private final EmailSenderService emailSenderService;
    private final UserMapper userMapper;

    @Override
    public Page<AppUser> findAll(int skip, int limit) {
        Pageable pageable = PageRequest.of(skip, limit);
        return appUserRepository.findAll(pageable);
    }

    @Override
    public AppUser findById(Long id, Boolean isEnable) {
        AppUser appUsersByIdAndIsEnable = appUserRepository.findAppUsersByIdAndIsEnable(id, isEnable);
        if (appUsersByIdAndIsEnable == null) {
            throw new RuntimeException(Messages.USER_CANT_FIND);
        }
        return appUsersByIdAndIsEnable;
    }

    @Override
    public ResponseEntity<String> updateAppUser(AppUser appUser) {
        AppUser appUsersByEmail = appUserRepository.findAppUsersByEmail(appUser.getEmail());
        if (appUsersByEmail == null) {
            throw new RuntimeException(Messages.USER_CANT_FIND);
        }
        appUserRepository.save(appUsersByEmail);
        return ResponseEntity.ok("Update successfully !");
    }

    @Override
    public ResponseEntity<String> deleteAppUser(String email) {
        AppUser appUsersByEmail = appUserRepository.findAppUsersByEmail(email);
        if (appUsersByEmail == null) {
            throw new RuntimeException(Messages.USER_CANT_FIND);
        }
        appUserRepository.deleteById(appUsersByEmail.getId());
        return ResponseEntity.ok("Delete successfully !");
    }

    @Override
    public ResponseEntity<String> saveAppUser(AppUserDto appUser) {
        AppUser appUsersByEmail = appUserRepository.findAppUsersByEmail(appUser.getEmail());
        if (appUsersByEmail != null) {
            throw new RuntimeException(Messages.EMAIL_IS_USING);
        }
        AppUser appUser1 = userMapper.userDtoToUser(appUser);
        appUserRepository.save(appUser1);
        emailSenderService.sendEmail(EmailDto.builder()
                .to(appUser.getEmail())
                .subject("Registration")
                .body("http://localhost:8080/appuser/confirmationEmail/" + appUser.getEmail())
                .build());
        return ResponseEntity.ok("Please confirmation your email !");
    }

    public ResponseEntity<String> emailConfirmation(String email) {
        AppUser appUsers = appUserRepository.findAppUsersByEmail(email);
        appUsers.setEnable(true);
        appUserRepository.save(appUsers);
        return ResponseEntity.ok("Save successfully !");
    }
}
