package com.company.paydaytrade;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
public class PaydayTradeApplication implements CommandLineRunner {
    //    private final ProductFeign feign;
    //    private final ProductServiceImpl productService;

    public static void main(String[] args) {
        SpringApplication.run(PaydayTradeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        ClientDto allAvailableCountry = feign.getAllAvailableCountry();
//        System.out.println(allAvailableCountry);
//        productService.saveProductWithFeign();

    }
}
